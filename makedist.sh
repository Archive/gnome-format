version=`grep VERSION= wscript | sed "s/'//g" | sed 's/=/ /g' | awk '{print $2}'`
wdir=gnome-format-${version}

rm -r $wdir
./waf dist
tar xjf gnome-format-$version.tar.bz2
cd $wdir
./waf distclean
./waf configure
./waf
mv build/default/src/*.[ch] src/
./waf distclean
rm makedist.sh

#remove vala dependency from wscript
cat wscript | sed 's/vala//g' > wscript.release
mv wscript.release wscript

cd ..
tar cjf gnome-format-$version.tar.bz2 $wdir/
rm -r $wdir
