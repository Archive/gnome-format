/*
 * Copyright © 2008 Michael Kanis <mkanis@gmx.de>
 *
 * This file is part of Gnome Format.
 *
 * Gnome Format is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gnome Format is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gnome Format.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Ped;


/*
PedExceptionOption
parted_exception_handler(PedException* ex) {
        if(error_message) g_free(error_message);
        error_message = g_strdup(ex->message);
        return PED_EXCEPTION_OK;
}
*/


namespace GnomeFormat {
        static void create_partition_2(string block_dev, string fs) {
                
                DiskType label_type;
                
//                ped_exception_set_handler(parted_exception_handler);
                 
                Device device = new Device(block_dev);
                Disk? disk = new Disk.from_device(device);
                
                if (disk == null) {
                        disk = new Disk(device, new DiskType("msdos"));
                }
                
                int last_part_num = disk.get_last_partition_num();
                if (last_part_num != -1) {
                        // if partitions exist, delete them
                        disk.delete_all();
                }

                Sector end = device.length - 1;

                FileSystemType fs_type = new FileSystemType(fs);
                
                // create new partition
                Partition part = new Partition(disk,
                        Ped.PartitionType.NORMAL, fs_type, 1, end);

                disk.add_partition(part, new Constraint.any(device));

                new FileSystem.create(&part.geom, fs_type, null);
                        
                // commit changes
                disk.commit_to_dev();
                
                // this needs root priviliges
                disk.commit_to_os();

                debug("device.sector_size: %lld", device.sector_size);
                debug("device.length: %lld", device.length);
                debug("end: %lld", end);
        }
}

