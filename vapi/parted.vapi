[CCode (cheader_filename = "parted/parted.h")]
namespace Ped {

        [CCode (unref_function = "ped_constraint_destroy")]
	public class Constraint {
	        [CCode (cname = "ped_constraint_any")]
	        public Constraint.any(Device device);
	}

        [CCode (unref_function = "ped_device_destroy")]
	public class Device {
                public int sector_size;
	        public Sector length;
	
	        [CCode (cname = "ped_device_get")]
	        public Device (string name);
	}

        [CCode (unref_function = "ped_disk_destroy")]
	public class Disk {
	        [CCode (cname = "ped_disk_new_fresh")]
	        public Disk(Device device, DiskType disk_type);

	        [CCode (cname = "ped_disk_new")]
	        public Disk.from_device(Device device);
	        
	        public int get_last_partition_num();
	        public int delete_all();
	        public int add_partition(Partition part, Constraint constraint);
	        public int commit_to_dev();
	        public int commit_to_os();
	}
	
        [CCode (unref_function = "")]
	public class DiskType {
	        [CCode (cname = "ped_disk_type_get")]
	        public DiskType(string name);
	}

        [SimpleType]
        [IntegerType]
	public struct Sector {
	}
	
        [CCode (unref_function = "")]
	public class Partition {
	        public Geometry geom;
	
	        [CCode (cname = "ped_partition_new")]
	        public Partition(Disk disk,
                                 PartitionType type,
                                 FileSystemType fs_type,
                                 Sector start,
                                 Sector end);
	}

        [CCode (cprefix = "PED_PARTITION_", cname = "_PedPartitionType")]
	public enum PartitionType {
                NORMAL,
                LOGICAL,
                EXTENDED,
                FREESPACE,
                METADATA,
                PROTECTED;
	}


        [CCode (unref_function = "")]
        public class FileSystem {
                [CCode (cname = "ped_file_system_create")]
                public FileSystem.create(Geometry *geom,
                                         FileSystemType type,
                                         Timer? timer);
        }
        
        [CCode (unref_function = "")]
        public class FileSystemType {
                [CCode (cname="ped_file_system_type_get")]
                public FileSystemType(string name);
        }
        
        public class Geometry {
        }
        
        public class Timer {
        }
}

